from django import forms
from django.forms import widgets
from user.models import Account, AccountMasyarakat
from django.contrib.auth import authenticate

CHOICES = [
    ("expired", "Product was expired"),
    ("insufficient", "Product amount was insufficient"),
    ("other", "Other")
]

class LoginForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = Account
        fields = ["telephone", "password"]

class ComplaintForm(forms.Form):
    complaint_category = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)
    complaint = forms.CharField(widget=forms.Textarea)