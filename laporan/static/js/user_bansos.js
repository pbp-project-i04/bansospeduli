setInterval(ajaxCall, 8000);
function ajaxCall() {
  $.ajax({
    dataType: "json",
    url: "/laporan/user-bansos/",
    success: function (data) {
      current_bansos = document.querySelectorAll(".bansos-id");
      all_unreported = document.querySelectorAll(".unreported .accordion-item").length
      current_bansos_id = [];
      if (current_bansos) {
        for (bansos of current_bansos) {
          try {
            current_bansos_id.push(bansos.innerText.substring(8, 44));
          } catch {
            current_bansos_id.push("-");
          }
        }
      } else {
        current_bansos_id = []
      }
      deleted_bansos = check_deleted_bansos(
        current_bansos_id,
        Object.keys(data)
      );
      if (deleted_bansos.length !== 0) {
        location.reload();
      } else {
        new_bansos = check_new_bansos(current_bansos_id, Object.keys(data));
        if (new_bansos.length !== 0) {
          display_new_bansos(
            new_bansos,
            data,
            all_unreported + new_bansos.length
          );
        }
      }
    },
  });
}
function check_deleted_bansos(all_bansos, requested_bansos) {
  let deleted_bansos = all_bansos.filter((x) => !requested_bansos.includes(x));
  return deleted_bansos;
}
function check_new_bansos(all_bansos, requested_bansos) {
  let new_bansos = requested_bansos.filter((x) => !all_bansos.includes(x));
  return new_bansos;
}

function display_new_bansos(new_bansos, data, num_of_bansos) {
  let accordion = document.querySelector(".unreported");
  for (id of new_bansos) {
    let accordion_item = document.createElement("div");
    accordion_item.classList.add("accordion-item");

    let accordionButton = document.createElement("button");
    accordionButton.classList.add("btn", "accordion-button", "bansos-id");
    accordionButton.setAttribute("data-bs-toggle", "collapse");
    accordionButton.setAttribute("data-bs-target", `#collapse${num_of_bansos}`);
    accordionButton.innerHTML = `
    Bansos #${id}<span> </span>&nbsp;&nbsp;&nbsp;<span class="subheading"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-circle" viewBox="0 0 16 16">
              <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
              <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
            </svg> click for more info</span>
    `;
    accordion_item.appendChild(accordionButton);

    let accordion_collapse = document.createElement("div");
    accordion_collapse.id = `collapse${num_of_bansos}`;
    accordion_collapse.classList.add("accordion-collapse", "collapse", "show");

    let accordion_body = document.createElement("div");
    accordion_body.classList.add("accordion-body");

    let report_button = document.createElement("span");
    report_button.classList.add("d-block", "mb-sm-3");
    let button = document.createElement("a");
    button.setAttribute("href", `/laporan/generate/${id}`);
    button.classList.add("btn", "btn-danger");
    button.innerText = "REPORT";
    report_button.appendChild(button);

    accordion_body.appendChild(report_button);

    let info = `<strong>Responsible Admin: </strong><span>${data[id].responsible_admin}</span><br>
        <h4><u>Info Bansos</u></h4>
        <strong>Provinsi: </strong><span>${data[id].provinsi}</span><br>
        <strong>Kecamatan: </strong><span>${data[id].kecamatan}</span><br>
        <strong>Kelurahan: </strong><span>${data[id].kelurahan}</span><br>
        <strong>Arrived On: </strong><span>${data[id].timestamp}</span><br>
        <h4><u>Bentuk Bantuan</u></h4>`;

    accordion_body.innerHTML += info;

    let bentuk_bantuan = document.createElement("table");
    bentuk_bantuan.classList.add("table");
    bentuk_bantuan.innerHTML =
      "<thead><tr><th scope='col'>#</th><th scope='col'>Bentuk</th><th scope='col'>Jumlah</th></tr></thead>";

    let bantuan = document.createElement("tbody");
    let bentuk_bantuan_data = data[id].bentuk_bantuan
    for (const [bentuk, info] of Object.entries(bentuk_bantuan_data)) {
      let row = document.createElement("tr");
      let header = document.createElement("th");
      let i = 0
      header.setAttribute("scope", "row");
      header.innerText = i + 1;
      row.appendChild(header);
      row.innerHTML += `<td>${bentuk}</td>
            <td>${info.jumlah} ${info.satuan}</td>`;
      bantuan.appendChild(row);
      i++;
    }
    bentuk_bantuan.appendChild(bantuan);
    accordion_body.appendChild(bentuk_bantuan);

    accordion_collapse.appendChild(accordion_body);

    accordion_item.appendChild(accordion_collapse);

    if (accordion) {
      accordion.prepend(accordion_item);
    } else {
      accordion = document.createElement("div");
      accordion.classList.add("accordion", "unreported");
      accordion.prepend(accordion_item);
      let container = document.querySelector(".notreported")
      container.appendChild(accordion)
    }
    num_of_bansos--;
  }
}
