from django.contrib import admin
from django.urls import include, path
from .views import *

urlpatterns = [
    path("generate/<uuid:id_bansos>", generate_report),
    path("", choose_bansos, name="laporan-lapor"),
    path("verify/", admin_view, name="laporan-admin"),
    path("verify/<uuid:laporan_id>/accepted=<status>", verify),
    path("user-bansos/", get_user_bansos),
    path("update/<uuid:laporan_id>", update_report),
    path("get-all-laporan/", laporan_info),
    path("get-assigned-laporan/", all_assigned_reports),
    path("flutter/get/all/bansos/<sessionid>/", get_all_user_bansos),
    path("flutter/get/bansos/<bansosid>/", get_user_bansos_info),
    path("flutter/get/all/<sessionid>/", get_all_user_laporan),
    path("flutter/get/<laporanid>/", get_user_laporan_info),
    path("flutter/post/<sessionid>/", post_user_laporan),
    path("flutter/patch/<laporanid>/", patch_user_laporan),
    path("flutter/get/all/admin/<sessionid>/", get_all_user_laporan_admin),
    path("flutter/patch/admin/<laporanid>/", patch_user_laporan_admin)
]
