from django import forms
from user.models import Account

# Create your models here.
class LoginForm(forms.ModelForm):
    password = forms.CharField(widget = forms.PasswordInput)
    class Meta:
        model = Account
        fields = ["telephone",
                  "password"]
