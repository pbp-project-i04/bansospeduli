setInterval(() => {
    $.ajax({
        dataType:"json", 
        url:"/beranda/covid_api",
        success:function(data){
            document.querySelector(".positif").innerText = data[0].positif
            document.querySelector(".sembuh").innerText = data[0].sembuh
            document.querySelector(".dirawat").innerText = data[0].dirawat
            document.querySelector(".meninggal").innerText = data[0].meninggal
        }
    })
}, 300000)