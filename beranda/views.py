from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm
from django.http import JsonResponse
from staff.models import Admin
import requests

def beranda(request):
    data = requests.get("https://api.kawalcorona.com/indonesia/")
    data_json = data.json()
    user = request.user
    context = {"data": data_json[0], "user":user}
    return render(request, "beranda.html", context)

def covid_api(request):
    data = requests.get("https://api.kawalcorona.com/indonesia/")
    data_json = data.json()
    return JsonResponse(data_json, safe = False)

def login_beranda(request):
    errors = []
    if request.user.is_authenticated:
        return redirect("beranda:beranda")
    if request.method == "POST":
        telephone = request.POST["telephone"]
        password = request.POST["password"]
        user = authenticate(request, telephone=telephone, password=password)

        if user:
            login(request, user)
            try:
                admin = user.admin
                return redirect("/staff/admin-info")
            
            except Admin.DoesNotExist:
                return redirect("/user/dashboard")
        else:
            errors.append("Invalid login.")

    form = LoginForm()
    context = {"form": form, "errors":errors}
    return render(request, "login-beranda.html", context)

def logout_account(request):
    logout(request)
    return redirect('beranda:beranda')