from django.contrib import admin
from .models import BansosGroup, Area, Bansos, BansosPreviousLocation

admin.site.register(BansosGroup)
admin.site.register(Area)
admin.site.register(Bansos)
admin.site.register(BansosPreviousLocation)