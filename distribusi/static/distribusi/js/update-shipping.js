const input_kelurahan = document.getElementById('new-kelurahan');
const input_kecamatan = document.getElementById('new-kecamatan');
const input_kabupaten_kota = document.getElementById('new-kabupaten-kota');
const input_provinsi = document.getElementById('new-provinsi');
const confirmation_button = document.getElementById('confirmation-update-button');

function updateConfirmation() {
    var provinsi_name = input_provinsi.value.split(",")[1];

    var str = "Update bansos location to " + input_kelurahan.value + ", " + input_kecamatan.value + ", " + 
        input_kabupaten_kota.value + ", " + provinsi_name + "?";
    
    if(!input_kelurahan.value || !input_kecamatan.value || !input_kabupaten_kota.value || !input_provinsi.value) {
        
        str = "Please complete all the fields";
        if(!confirmation_button.classList.contains("disabled")) {
            confirmation_button.classList.add("disabled");
        }

    } else {
        if(confirmation_button.classList.contains("disabled")) {
            confirmation_button.classList.remove("disabled");
        }
    }
    
    document.getElementById('confirmation-location-info').innerText = str;

}