// id_counter to uniquely identify dynamically generated input
var id_counter = 0;

const area_selector = document.getElementById('area');
const area_code_placeholder = document.getElementById('area-code-placeholder');
const area_name_placeholder = document.getElementById('area-name-placeholder');
const area_number_rec_placeholder = document.getElementById('area-number-rec-placeholder');
const bansos_history_table = document.getElementById('bansos-history-table');
const confirmation_button = document.getElementById("confirmation-generate-button");

area_selector.addEventListener('change', areaInfoListener);

function get_area_data() {
    var area_dictionary;
    $.ajax({
        async:false,
        type: 'GET',
        url: '/distribusi/get-area-information/',
        success: (data) => {
            area_dictionary = data;
        }
    });

    return area_dictionary;
}

function areaInfoListener() {
    var area_code = area_selector.value;
    var area_dictionary = get_area_data();

    area_code_placeholder.innerHTML = area_dictionary[area_code]["area_code"];
    area_name_placeholder.innerHTML = area_dictionary[area_code]["area_name"];
    area_number_rec_placeholder.innerHTML = area_dictionary[area_code]["number_of_receiver"];
    assigned_bansos = area_dictionary[area_code]["assigned_bansos"];

    var table_string = "";
    var new_row;
    var bansos_column;
    var status_column;
    var info_column;
    var info_a_tag;

    bansos_history_table.innerText = "";
    for(let i = 0; i < assigned_bansos.length; i++) {
        new_row = document.createElement('tr');

        bansos_column = document.createElement('td');
        bansos_column.innerText = assigned_bansos[i][0];

        status_column = document.createElement('td');
        status_column.innerText = assigned_bansos[i][1];

        info_column = document.createElement('td');
        info_a_tag = document.createElement('a');
        info_a_tag.setAttribute('target', '_blank');
        info_a_tag.setAttribute('href', "/distribusi/detail-bansos/"+assigned_bansos[i][0]);
        info_a_tag.innerText = 'More Info';
        info_column.appendChild(info_a_tag);

        new_row.appendChild(bansos_column);
        new_row.appendChild(status_column);
        new_row.appendChild(info_column);

        bansos_history_table.appendChild(new_row);
    }

}

function addBansos() {
    // Increment counter when add button is clicked (addBansos is called)
    id_counter++;

    // Create new tr (table row element)
    const bansosTypeContainer = document.createElement('tr');

    // Set ID of row to row(id_counter) --> Used for deletion
    bansosTypeContainer.setAttribute("id", "row".concat(id_counter.toString()));
    
    // Generate strings as innerHTML of row, each column consist of an input, the can be referenced by
    // id --> type_input(id_counter) and
    // id --> type_value(id_counter)

    // Create td for type_input
    var td_input_type = document.createElement("td");
    // Create type_input
    var input_type = document.createElement("input");
    input_type.setAttribute('type', 'text');
    input_type.setAttribute('class', 'form-control-plaintext');
    input_type.setAttribute('placeholder', 'Type');
    input_type.setAttribute('id', "type_input"+id_counter.toString());
    input_type.setAttribute('name', "type_input"+id_counter.toString());
    td_input_type.appendChild(input_type);

    // Create td for type_value
    var td_input_value = document.createElement("td");
    // Create type_value
    var input_value = document.createElement("input");
    input_value.setAttribute('type', 'number');
    input_value.setAttribute('class', 'form-control-plaintext');
    input_value.setAttribute('placeholder', 'Value');
    input_value.setAttribute('id', "type_value" + id_counter.toString());
    input_value.setAttribute('name', "type_value" + id_counter.toString());

    td_input_value.appendChild(input_value);

    // Create td for type_unit
    var td_input_unit = document.createElement("td");
    // Create input_unit
    var input_unit = document.createElement("input");
    input_unit.setAttribute('type', 'text');
    input_unit.setAttribute('class', 'form-control-plaintext');
    input_unit.setAttribute('placeholder', 'Unit');
    input_unit.setAttribute('id', "type_unit" + id_counter.toString());
    input_unit.setAttribute('name', "type_unit" + id_counter.toString());

    td_input_unit.appendChild(input_unit);

    // Create td for button
    var td_button = document.createElement("td");

    // Create input for button
    var button_remove = document.createElement("input");
    button_remove.setAttribute('type', 'button');
    button_remove.setAttribute('class', 'btn btn-danger btn-sm');
    button_remove.setAttribute('value', 'Remove');
    button_remove.setAttribute('onclick', "removeBansos('" + "row" + id_counter + "')")

    td_button.append(button_remove);

    bansosTypeContainer.append(td_input_type);
    bansosTypeContainer.append(td_input_value);
    bansosTypeContainer.append(td_input_unit);
    bansosTypeContainer.append(td_button);
    
    // Set hidden item-counter input value to id_counter
    // This will be used in keeping track of the number of bansos type added
    document.getElementById('item-counter').value = id_counter;

    // Add new row to container-bansos
    document.getElementById('container-bansos-body').appendChild(bansosTypeContainer);
}

function removeBansos(row_id) {
    // Get corresponding row from row_id
    var row_to_be_removed = document.getElementById(row_id);

    // Remove
    row_to_be_removed.remove();
}

function generateConfirmation() {
    // Function to confirm in generating bansos. After creation, bansos group cannot be deleted.
    const input_kelurahan = document.getElementById('input_kelurahan_target');
    const input_kecamatan = document.getElementById('input_kecamatan_target');
    const input_kabupaten_kota = document.getElementById('input_kabupaten_kota_target');
    const input_provinsi = document.getElementById('area');

    const input_kelurahan_origin = document.getElementById('input_kelurahan');
    const input_kecamatan_origin = document.getElementById('input_kecamatan');
    const input_kabupaten_kota_origin = document.getElementById('input_kabupaten_kota');
    
    var str = "Generate bansos to " + input_kelurahan.value + ", " + input_kecamatan.value + ", " + 
        input_kabupaten_kota.value + ", " + input_provinsi.value + "?";
    
    if(!input_kelurahan.value || !input_kecamatan.value || !input_kabupaten_kota.value || !input_provinsi.value ||
         !input_kelurahan_origin.value || !input_kecamatan_origin.value || !input_kabupaten_kota_origin.value) {
        str = "Please complete all the fields";
        if(!confirmation_button.classList.contains("disabled")) {
            confirmation_button.classList.add("disabled");
        }

    } else {
        if(confirmation_button.classList.contains("disabled")) {
            confirmation_button.classList.remove("disabled");
        }
    }
    
    document.getElementById('confirmation-location-info').innerText = str;
}


function parseDataFromScript() {
    var area_dictionary = {};
    
    // Get all area data classes
    var area_data = document.getElementsByClassName('area_data');

    for(let i = 0; i < area_data.length; i++) {
        var parsed_full = JSON.parse(area_data[i].textContent);
        
        parsed_full["assigned_bansos"] = JSON.parse(parsed_full["assigned_bansos"]).slice(0, -1);

        parsed_full["assigned_bansos_status"] = JSON.parse(parsed_full["assigned_bansos_status"]).slice(0, -1);

        area_dictionary[parsed_full["area_code"]] = parsed_full;


    }

    return area_dictionary;
}




