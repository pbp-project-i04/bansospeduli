/* AJAX Section to update the location */

$(document).ready(function() {
    data_updater();
})

const past_location_container = $("#past-location-container");
const bansos_group_id = $("#bansos-group-id").text();

function search_matching_json(data, group_id) {
    var final_result = [];
    for(let i = 0; i < data.length; i++) {
        var iter_bansos_group_fields = data[i].fields;
        var iter_bansos_group_id = iter_bansos_group_fields.bansos_group;
        var matching_fields = null;
        
        if(group_id == iter_bansos_group_id) {
            matching_fields = data[i].fields;
            final_result.push(matching_fields);
        } 
    }
    return final_result;
}

// AJAX method to constantly update previous location data
function data_updater() {
    $.ajax({
        type: 'GET',
        // Get JSON data 
        url: '/distribusi/get-previous-location/',

        // Update data when json fetching is successful
        success: function(data) {
            var bansos_group_locations_data = search_matching_json(data, bansos_group_id);

            // Generate row-per-row data

            past_location_container.innerText = "";

            var row_element;
            var status_column;
            var kelurahan_column;
            var kecamatan_column;
            var kabupaten_kota_column;
            var provinsi_column;
            var time_stamp_column;

            // Create table head
            var table_previous = document.createElement('table');
            table_previous.setAttribute('class', 'table');

            var table_head = document.createElement('thead');
            table_head.setAttribute('class', 'thead-dark');

            row_element = document.createElement('tr');

            var th_status = document.createElement('th');
            th_status.innerText = 'Status';

            var th_kelurahan = document.createElement('th');
            th_kelurahan.innerText = 'Past Urban Village';

            var th_kecamatan = document.createElement('th');
            th_kecamatan.innerText = 'Past Sub-District';

            var th_kabupaten_kota = document.createElement('th');
            th_kabupaten_kota.innerText = 'Past District/City';

            var th_provinsi = document.createElement('th');
            th_provinsi.innerText = 'Past Province';

            var th_time_stamp = document.createElement('th');
            th_time_stamp.innerText = 'Time Stamp';

            var time_stamp_bansos;

            row_element.appendChild(th_status);
            row_element.appendChild(th_kelurahan);
            row_element.appendChild(th_kecamatan);
            row_element.appendChild(th_kabupaten_kota);
            row_element.appendChild(th_provinsi);
            row_element.appendChild(th_time_stamp);

            table_head.appendChild(row_element);

            var table_body = document.createElement('tbody');

            // Reconstruct each rows
            for(let i = 0; i < bansos_group_locations_data.length; i++) {
                row_element = document.createElement('tr');

                status_column = document.createElement('td');
                status_column.innerText = bansos_group_locations_data[i].status;

                kelurahan_column = document.createElement('td');
                kelurahan_column.innerText =  bansos_group_locations_data[i].kelurahan;

                kecamatan_column = document.createElement('td');
                kecamatan_column.innerText = bansos_group_locations_data[i].kecamatan;

                kabupaten_kota_column = document.createElement('td');
                kabupaten_kota_column.innerText = bansos_group_locations_data[i].kabupaten_kota;

                provinsi_column = document.createElement('td');
                provinsi_column.innerText = bansos_group_locations_data[i].provinsi;

                time_stamp_column = document.createElement('td');
                time_stamp_bansos = new Date(bansos_group_locations_data[i].time_stamp);
                
                time_stamp_column.innerText = time_stamp_bansos.toString();

                row_element.appendChild(status_column);
                row_element.appendChild(kelurahan_column);
                row_element.appendChild(kecamatan_column);
                row_element.appendChild(kabupaten_kota_column);
                row_element.appendChild(provinsi_column);
                row_element.appendChild(time_stamp_column);

                table_body.appendChild(row_element);

            }

            // Replace HTML content with the newly generated content
            
            table_previous.appendChild(table_head);
            table_previous.appendChild(table_body);

            past_location_container.html(table_previous);
                
            // Refresh function every 500ms
            setTimeout(data_updater, 5000);
        }
    });
}

