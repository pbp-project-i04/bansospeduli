from django.contrib.auth import login
from django.http.request import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from user.models import Account
from staff.models import Admin
from distribusi.models import Area, BansosGroup
from .views import bansos_group_detail

class DistribusiTestCase(TestCase):
    # Check login required
    def test_distribusi_is_login_required(self):
        response = Client().get('/distribusi/')
        self.assertEqual(response.status_code, 302)
    
    def test_tracking_dashboard_is_login_required(self):
        response = Client().get('/distribusi/tracking-admin-dashboard/')
        self.assertEqual(response.status_code, 302)
    
    def test_generate_bansos_is_login_required(self):
        response = Client().get('/distribusi/generate-bansos/')
        self.assertEqual(response.status_code, 302)
    
    def test_register_area_is_login_required(self):
        response = Client().get('/distribusi/register-area/')
        self.assertEqual(response.status_code, 302)

    # Check 200 with login (Generating Admin Endpoints Check)
    def test_all_bansos(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')
        response = self.client.get('/distribusi/')
        self.assertEqual(response.status_code, 200)
    
    def test_generate_bansos(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')
        response = self.client.get('/distribusi/generate-bansos/')
        self.assertEqual(response.status_code, 200)
    
    def test_generate_area(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')
        response = self.client.get('/distribusi/register-area/')
        self.assertEqual(response.status_code, 200)

    def test_bansos_group_detail(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='queued',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )
        
        bansos_to_test.save()
        
        response = self.client.get('/distribusi/detail-bansos/'+str(bansos_to_test.id_bansos_group)+"/")
        self.assertEqual(response.status_code, 200)

    # Check 200 with login (Tracking Admin Endpoints Check)
    def test_tracking_admin_dashboard(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        tracking_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        tracking_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')

        response = self.client.get('/distribusi/tracking-admin-dashboard/')
        self.assertEqual(response.status_code, 200)
    
    def test_tracking_admin_bansos_update(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='queued',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )
        
        bansos_to_test.save()

        response = self.client.get('/distribusi/update-shipping/'+str(bansos_to_test.id_bansos_group)+"/")
        self.assertEqual(response.status_code, 200)
    

    # TEST FORBIDDEN
    def test_distribusi_as_tracking_admin(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')
        
        response = self.client.get('/distribusi/')

        self.assertEqual(response.status_code, 403)

    def test_generate_bansos_as_tracking_admin(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')
        response = self.client.get('/distribusi/generate-bansos/')
        self.assertEqual(response.status_code, 403)

    def test_register_area_as_tracker(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')
        response = self.client.get('/distribusi/register-area/')
        self.assertEqual(response.status_code, 403)

    def test_bansos_detail_as_tracker(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='queued',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )
        
        bansos_to_test.save()
        
        response = self.client.get('/distribusi/detail-bansos/'+str(bansos_to_test.id_bansos_group)+"/")
        self.assertEqual(response.status_code, 403)

    def test_tracking_admin_dashboard_as_generator(self):
        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()
        self.client.login(telephone='082312330001', password='helloworld')
        
        response = self.client.get('/distribusi/tracking-admin-dashboard/')

        self.assertEqual(response.status_code, 403)

    def test_update_shipping_as_generator(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='generator', name='Winaldo')
        self.account.save()
        generator_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        bansos_to_test = BansosGroup(
                responsible_admin=generator_admin,
                receiving_provinsi=new_area,
                status='queued',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )
        
        bansos_to_test.save()

        response = self.client.get('/distribusi/update-shipping/'+str(bansos_to_test.id_bansos_group)+"/")
        self.assertEqual(response.status_code, 403)
    
    def test_update_shipping_as_different_tracker(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        account = Account(telephone='082312330002')
        account.set_password('helloworld')
        another_admin = Admin(owning_user=account, type='tracker', name='Winaldo')
        account.save()
        another_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        bansos_to_test = BansosGroup(
                responsible_admin=another_admin,
                receiving_provinsi=new_area,
                status='queued',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )
        
        bansos_to_test.save()

        response = self.client.get('/distribusi/update-shipping/'+str(bansos_to_test.id_bansos_group)+"/")
        self.assertEqual(response.status_code, 403)
    
    # Model Test
    def test_bansos_group(self):
        new_area = Area(area_code='JKT', area_name='JAKARTA')
        new_area.save()

        self.client = Client()
        self.account = Account(telephone='082312330001')
        self.account.set_password('helloworld')
        generator_admin = Admin(owning_user=self.account, type='tracker', name='Winaldo')
        self.account.save()
        generator_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        account = Account(telephone='082312330002')
        account.set_password('helloworld')
        another_admin = Admin(owning_user=account, type='tracker', name='Winaldo')
        account.save()
        another_admin.save()

        self.client.login(telephone='082312330001', password='helloworld')

        bansos_to_test = BansosGroup(
                responsible_admin=another_admin,
                receiving_provinsi=new_area,
                status='queued',
                kelurahan='Sample Kelurahan',
                kecamatan='Sample Kecamatan',
                kabupaten_kota='Sample Kota',
                provinsi='Sample Provinsi',
                destination_kelurahan='target_kelurahan',
                destination_kecamatan='target_kecamatan',
                destination_kabupaten_kota='target_kabupaten_kota',
                total_receiver=54545,
                bentuk_bantuan={'jagung':'3'},
                satuan_bantuan={'jagung':'buah'},
            )
        
        bansos_to_test.save()

        self.assertEqual(bansos_to_test.responsible_admin, another_admin)
        self.assertEqual(bansos_to_test.receiving_provinsi, new_area)
        self.assertEqual(bansos_to_test.status, 'queued')
        self.assertEqual(bansos_to_test.kelurahan, 'Sample Kelurahan')
        self.assertEqual(bansos_to_test.kecamatan, 'Sample Kecamatan')
        self.assertEqual(bansos_to_test.kabupaten_kota, 'Sample Kota')
        self.assertEqual(bansos_to_test.provinsi, 'Sample Provinsi')
        self.assertEqual(bansos_to_test.destination_kelurahan, 'target_kelurahan')
        self.assertEqual(bansos_to_test.destination_kecamatan, 'target_kecamatan')
        self.assertEqual(bansos_to_test.destination_kabupaten_kota, 'target_kabupaten_kota')
        self.assertEqual(bansos_to_test.total_receiver, 54545)
        self.assertEqual(bansos_to_test.bentuk_bantuan, {'jagung':'3'})
        self.assertEqual(bansos_to_test.satuan_bantuan, {'jagung':'buah'})
    
    



