from django.contrib.auth import login
from django.http.request import HttpRequest
from django.test import TestCase
from django.test import Client

class RegistrationTestCase(TestCase):
    # Check login required
    def test_register_citizen(self):
        response = Client().get('/registration/citizen/')
        self.assertEqual(response.status_code, 200)
